# Translate Me Senpai - POC

Fonctionnalité de traduction instantané d'image via URL.
Requiert: Node v10.14.1 ou supérieur

API utilisé:
-Free OCR API 
-Google Translate API
 
Configuration:
Dans Config.js, spécifier l'url de l'image à traduire et l'apikey (activation via google API translate)

Lancer:
Dans le dossier racine, lancer une fenêtre de commande et taper "node script.js"