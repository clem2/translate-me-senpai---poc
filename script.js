const https = require('https');
var fs = require("fs");
var tools = require('./translate');
var config = require('./config.js');
//var base64image = fs.readFileSync('./a.jpg', 'base64'); => convert to base6image

const callback = async function (error, response, body) {
  // le résultat de la requete OCR est utilisé pour une seconde requete vers l'api Google Translate
  console.log("----------------------------------------\n");
  console.log(await tools.translate(response) + "\n");
  console.log("----------------------------------------");
};


getTexteByImageUrl(config, callback);
//s'éxécute puis appelle la callback
console.log("Traduction en cours . . .\n");
// s'éxécute avant

function getTexteByImageUrl(config, callback) {
  // Eequete à l'api OCR avec les parametre de .config
  var GetUrl = 'https://api.ocr.space/parse/imageurl?apikey=' + config.apikey + '&url=' + config.url + '&isOverlayRequired=true&language=' + config.lang;
  https.get(GetUrl, (resp) => {
    let data = "";
    resp.on('data', (chunk) => {
      data += chunk;
    });

    resp.on('end', () => {
      //console.log(JSON.parse(data));
      if(typeof JSON.parse(data).ParsedResults[0] == undefined){
        return callback(null,"Erreur.",null);
      }
      return callback(null, nettoyer_Msg(JSON.parse(data).ParsedResults[0].ParsedText), null);
    });

  }).on("error", (err) => {
    console.log("Error: " + err.message);
    return;
  });
}

// FONCTIONS
function nettoyer_Msg(brut) {
  //On supprime les sauts de lignes
  var tab = brut.split("\r\n");
  var string = "";
  for (i = 0; i < tab.length; i++) {
    string += tab[i];
  }
  return string
}